"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np

from src.forward_model import CFA
from src.methods.template.somefunc import spectral_difference, normalization, quad_bayer_to_bayer_pattern, quad_bayer_to_bayer_image, bilinear_interpolation


def run_reconstruction(y: np.ndarray, cfa: str) -> np.ndarray:
    """Performs demosaicking on y.

    Args:
        y (np.ndarray): Mosaicked image to be reconstructed.
        cfa (str): Name of the CFA. Can be bayer or quad_bayer.

    Returns:
        np.ndarray: Demosaicked image.
    """
    # Performing the reconstruction.
    
    input_shape = (y.shape[0], y.shape[1], 3)
    op = CFA(cfa, input_shape)

    if op.cfa != 'bayer' and op.cfa != 'quad_bayer':
        raise ValueError("CFA must be bayer or quad_bayer")
    
    if op.cfa == 'quad_bayer':
        quad_bayer_to_bayer_pattern(op)
        quad_bayer_to_bayer_image(y)

    # Apply adjoint operation on raw aquisition
    z = op.adjoint(y)

    # Demosaicing process
    res_bi = bilinear_interpolation(op, z)
    res_sd = spectral_difference(op, z, res_bi)
    res = normalization(res_sd) # min-max normalization

    return res


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
